import axios from "axios";
// const URL = "http://127.0.0.1:8000";
const URL = "https://bi.cantaws.net.br"; //https://bi.cantaws.net.br/api/ http://127.0.0.1:8000/api
// const URL = "https://api.cantaws.net.br";

const axiosInstance = axios.create({
  baseURL: `${URL}/api`,
  headers : {
    "Access-Control-Allow-Headers": "*"
  }  
});

axiosInstance.interceptors.request.use(
    function (config) {
        config.headers.Authorization = 123;
        return config;
    },
    function (successRes) {
      console.log(successRes);
    },
    function (error) {
      return Promise.reject(error);
    }
  );

export const api = {
    get(endpoint) {
      return axiosInstance.get(endpoint);
    },
    post(endpoint, body) {
      return axiosInstance.post(endpoint, body);
    },
    put(endpoint, body) {
      return axiosInstance.put(endpoint, body);
    },
    delete(endpoint) {
      return axiosInstance.delete(endpoint);
    }    
};
