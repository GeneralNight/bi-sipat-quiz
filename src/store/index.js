import Vue from 'vue'
import Vuex from 'vuex'
import { api } from '@/services.js';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      id: null,
      name: '',
      departament: '',
      results: Array(),
      got : 0
    },
    quiz: {
      campaign : {
        id: 1,
        name: '',
      },
      questions: [
        
      ]
    },
    logged: false,

  },
  mutations: {
    CLEAR_USER(state) {
      state.user = {
        id: null,
        name: null,
        departament: null,
        results: Array(),
        got : 0
      }
    },
    UPDATE_USER_ID(state,payload) {
      state.user.id = payload.user_id;
    },
    UPDATE_USER_INFO(state, payload) {
      state.user.name = payload.name
      state.user.email = payload.email,
      state.user.departament = payload.departament
    },
    UPDATE_USER_QUESTIONS(state, payload) {
      state.user.results = payload.results;
    },
    UPDATE_LOGGED(state) {
      state.logged = !state.logged;
    },
    UPDATE_QUIZ_ID(state, payload) {
      state.quizId = payload.quizId;
    },
    UPDATE_QUESTIONS(state, payload) {
      state.quiz.questions = payload.questions
    },
    UPDATE_GOT(state,payload) {
      state.user.got = payload.got
    },
    RESET_LOGGED(state) {
      state.logged = false;
    },
    RESET_QUIZ(state) {
      state.quiz.questions = [];
    }
  },
  actions: {
    async getQuestions() {
      return await api.get(`/campaign`).then(response => { // /campaign
        console.log(response.data)
        try {
          if(response.data.campaign.id != 0) {
            this.state.quiz = response.data;
            return 'ok'
          }
        } catch (error) {
          return 'error'
        }        
      });
    //  return api.post(`/auth/login`, {});
    }
  },
  modules: {
  }
})
